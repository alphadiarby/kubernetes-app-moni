E-COMMERCE-KUBERNETES:
---------------------

Dans ce projet on deploie un site web avec kubernetes pour cela dans un premier il faudra créer une image docker qui sera heberger sur docker hub. 

Dockerfile:
-----------

le dockerfile nous creéra l'image faudra lancer la commande:

** docker build -t <name> .

Kubernetes:
-----------

Persitent-volume:
----------------

on alloue un espace qu'on définit comme volume qui se situera au noeud dans le repertoire /data/html

kubectl create -f persistent.yaml

Persistent-volume-Claim:
-----------------------

on utilisera une partie du volume.

kubectl create -f claims.yaml

secrets:
----------

pour que le front-end se connecte à la base de donnée on lui fournit une variable du type DATABASE_URL

kubectl create secret generic <non-confimaps: mountdb > --from-file=DATABASE_URL=mysql:<l'url>

on crée ensuite un secret pour les env de mysql 

kubectl create secret generic <non-confimaps: mountdb > --from-literal=MYSQL_PASSWORD=<value>

tu peux ajoutes d'autres --from-literal=<key>=<value> de preference je crée puis editer afin d'ajouter mes variables 

**important** faudra coder les values en base64 ex: echo -n "value" | base64

Configmaps:
----------


je crée un configmaps afin de monter mon script sql 

kubectl create configmpas <nom> --from-file=<fichier>

une fois l'image crée, on aura besoin d'un front-end et un back-end

front-end:
----------

le fichier **front.yaml** crée un deploieement du front avec l'image docker crée. Pour lancer la création :

kubectl create -f front.yaml

Ensuite on lui crée un service de type NodePOrt pour l'exposer et qu'il soit accessible.

kubectl create -f service-front.yaml

back-end:
---------


le fichier **mysql.yaml** crée un deploieement du back avec l'image mysql. Pour lancer la création :

kubectl create -f mysql.yaml

Ensuite on lui crée un service de type CLUSTERIP .

kubectl create -f service-mysql.yaml



  